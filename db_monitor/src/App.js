import React from 'react';
import './App.css';
import './bootstrap.min.css';
import { Button, InputGroup,FormControl } from 'react-bootstrap';
const axios = require('axios');

function App() {
    return (
      <div className="App">
        <ContactForm/>
        <FilterableContactTable contacts={CONTACTS} />
      </div>
    );
}

export default App;


class ContactRow extends React.Component {
  constructor(props) {
    super(props);
    this.changeUser = this.changeUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.inputName = this.props.contact.name;
    this.inputPhone = this.props.contact.phone;
    this.inputNameRef = React.createRef(); 
    this.inputPhoneRef = React.createRef();
    this.state = {
      changed: false
    } 
  }
  
  changeUser() {
    this.setState({changed: false});
    axios.post(`http://localhost:8080/phone/${this.props.contact.id}`,{
      username:this.inputName,
      userphone:this.inputPhone
    })
  }
  handleChange() {
    this.setState({changed: true});
    this.inputName = this.inputNameRef.current.value;
    this.inputPhone = this.inputPhoneRef.current.value;
 }

  deleteUser() {
    const url = `http://localhost:8080/phone/${this.props.contact.id}`
    axios.delete(url)
    .then(res =>{
      console.log("deleted")
    })
    .catch(err => {
      
    });
  }


  render() {
    return (
      <tr>
        <td>
          <InputGroup className="mb-3">
            <FormControl
              defaultValue={this.props.contact.name} 
              ref={this.inputNameRef} type="text" onChange={() => this.handleChange()}
            />
          </InputGroup>
        </td>
        <td>
        <InputGroup className="mb-3">
            <FormControl
              defaultValue={this.props.contact.phone}
              ref={this.inputPhoneRef} type="text" onChange={() => this.handleChange()}
            />
          </InputGroup>
        </td>
        <td>{this.props.contact.id}</td>
        <td>
          <Button disabled={!this.state.changed} onClick={() => this.changeUser()} variant="light">Изменить</Button>{' '}
          <Button onClick={() => this.deleteUser()} variant="outline-danger">Удалить</Button>{' '}
        </td>
      </tr>
    );
  }
}

class ContactTable extends React.Component {
  render() {
    var rows = [];
    this.props.contacts.forEach((contact) => {
      if (contact.name.indexOf(this.props.filterText) === -1 &&
          contact.phone.indexOf(this.props.filterText) === -1) {
        return;
      }
      rows.push(<ContactRow contact={contact} />);
    });
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Phone</th>
            <th>id</th>
            <th>Действия</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.handleFilterTextInputChange = this.handleFilterTextInputChange.bind(this);
  }
  
  handleFilterTextInputChange(e) {
    this.props.onFilterTextInput(e.target.value);
  }

  render() {
    return (
      <form>
        <input
          className="form-control"
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextInputChange}
        />
      </form>
    );
  }
}

class FilterableContactTable extends React.Component {
  constructor(props) {
    super(props);
    // FilterableContactTable is the owner of the state as the filterText is needed in both nodes (searchbar and table) that are below in the hierarchy tree.
    this.state = {
      filterText: '',
      contacts:[]
    };
    
    this.handleFilterTextInput = this.handleFilterTextInput.bind(this);
    
  }

  handleFilterTextInput(filterText) {
    //Call to setState to update the UI
    this.setState({
      filterText: filterText
    });
    //React knows the state has changed, and calls render() method again to learn what should be on the screen
  }

  async componentDidMount() { 
    let usersArray = []
    axios.get("http://localhost:8080/phones/")
    .then(res =>{
      console.log(res)
      let data = res.data;
      data.forEach(element => {
        usersArray.push({
          name: element.username,
          phone: element.userphone,
          id: element.id
        })
      });  
      this.setState({contacts: usersArray})
    }) 
  }
  
  
  render() {
    return (
      <div>
        <h1>Users in table</h1>
        <SearchBar
          filterText={this.state.filterText}
          onFilterTextInput={this.handleFilterTextInput}
        />
        <ContactTable
          contacts={this.state.contacts}
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}
class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      phone:''
    };

    this.handleChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    //alert('A form was submitted: ' + this.state.name + ' // ' + this.state.phone);
    event.preventDefault();
    axios.post("http://localhost:8080/phones/createNewPhone",{
      users:[{"username":this.state.name,"userphone":this.state.phone}]
    })
    .then(res =>{
      console.log(res)
    }) 
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} >
          <div className="form-group">
            <label for="nameImput">Name</label>
            <input type="text" name="name" value={this.state.name} onChange={this.handleChange} className="form-control" id="nameImput" placeholder="Name" />
          </div>
          <div className="form-group">
            <label for="phoneImput">Phone</label>
            <input name="phone" type="phone" value={this.state.phone} onChange={this.handleChange} className="form-control" id="phoneInput" placeholder="Phone" />
          </div>
          <input type="submit" value="Добавить в базу" className="btn btn-primary"/>
        </form>
      </div>
    )
  }
}
var CONTACTS = [
  {name: 'Emma Page', phone: '555-444-333'}
];
/*
ReactDOM.render(
  <FilterableContactTable contacts={CONTACTS} />,
  document.getElementById('container')
);*/