package DB

import Models.UserD
import scalikejdbc._
import com.typesafe.config.{Config, ConfigFactory}

object DBWorker{
  val config: Config = ConfigFactory.load("app.conf")
  config.atKey("database.driver")
  val driver = config.getString("database.driver")
  val url = config.getString("database.url")
  val username = config.getString("database.user")
  val password = config.getString("database.password")


  Class.forName(driver)
  ConnectionPool.singleton(url, username, password)

  implicit val session = AutoSession

  def initdb()(implicit s: DBSession = AutoSession): Boolean = {
    sql"CREATE TABLE test.`phones` ( id int(11) auto_increment NOT NULL PRIMARY KEY, name varchar(100) NOT NULL, phone varchar(100) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;".update.apply
    true
  }

  def create(name: String, phone: String)(implicit s: DBSession = AutoSession): Long = {
    var id = sql"insert into phones values (0, ${name},${phone})"
      .updateAndReturnGeneratedKey.apply() // returns auto-incremeneted id
    id
  }

  def getUser(id: Long)(implicit s: DBSession = AutoSession): Option[UserD] = {
    sql"select * from phones where id = ${id}".map(x => new UserD(x.string("name"),x.string("phone"),x.string("id")) ).first.apply
  }

  def deleteUser(id: Long)(implicit s: DBSession = AutoSession): Boolean = {
    sql"DELETE FROM phones WHERE id = ${id}".update.apply
    true
  }

  def getUsersByName(value: String)(implicit s: DBSession = AutoSession): List[UserD] = {
    sql"select * from phones where locate(${value}, name)".map(x => new UserD(x.string("name"),x.string("phone"),x.string("id")) ).list.apply
  }
  def getUsersByPhone(value: String)(implicit s: DBSession = AutoSession): List[UserD] = {
    sql"select * from phones where locate(${value}, phone)".map(x => new UserD(x.string("name"),x.string("phone"),x.string("id")) ).list.apply
  }

  def getAllUsers()(implicit s: DBSession = AutoSession): List[UserD] = {
    sql"select * from phones".map(x => new UserD(x.string("name"),x.string("phone"),x.string("id")) ).list.apply
  }
  def updateUser(id: Long, name: String, phone: String)(implicit s: DBSession = AutoSession): Boolean = {
    sql"update phones set name = ${name}, phone = ${phone} WHERE id = ${id}".update.apply()
    true
  }
}