package Models

class UserD(name: String, phone: String, myid: String){
  var username: String = name
  var userphone: String = phone
  var id: String = myid

  def toJSONString: String = {
    "{" +
      "\"username\":\""  + this.username +  "\"" + "," +
      "\"userphone\":\"" + this.userphone + "\"" + "," +
      "\"id\":\""        + this.id +        "\"" +
    "}"
  }
}

class DataHelper{
  def getJSONArray(list: List[UserD]): String = {
    val x = new StringBuilder("[")
    var prefix = ""
    for (e <- list){
      x.append(prefix)
      x.append(e.toJSONString)
      prefix = ","
    }
    x.append("]")
    x.toString()
  }
}