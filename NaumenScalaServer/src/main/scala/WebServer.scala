import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.Done
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives.{post, _}


import scala.util.parsing.json.{JSONArray}
import Models.{DataHelper, UserD}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn
import scala.concurrent.Future

object WebServer {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher
  private val cors = new CORSHandler {}


  final case class User(username: String, userphone: String)
  final case class UserPayload(users: List[User])


  implicit val userFormat = jsonFormat2(User)
  implicit val payloadFormat = jsonFormat1(UserPayload)

  def addUsers(payload: UserPayload) : Future[List[Long]] = {
    var ids: List[Long] = List()
    for(e <- payload.users){
      val id = DB.DBWorker.create(e.username, e.userphone)
      ids:+=(id)
    }
    Future{ids}
  }

  def getUser(id: Long) : Future[Option[UserD]] = {
    Console.print(id)
    var user = DB.DBWorker.getUser(id.toLong)
    Console.print(user)
    Future{user}
  }

  def deleteUser(id: Long) : Future[Done] = {
    DB.DBWorker.deleteUser(id)
    Future{Done}
  }

  def getUserByName(value: String): Future[List[UserD]] = {
    var result = DB.DBWorker.getUsersByName(value)
    Future{result}
  }
  def getUsers(): Future[List[UserD]] = {
    var result = DB.DBWorker.getAllUsers()
    Future{result}
  }

  def getUserByPhone(value: String): Future[List[UserD]] = {
    var result = DB.DBWorker.getUsersByPhone(value)
    Future{result}
  }

  def updateUser(id: Long, user: User): Future[Done] ={
    DB.DBWorker.updateUser(id, user.username, user.userphone)
    Future{Done}
  }

  def main(args: Array[String]) {
    val route: Route =
      concat(
        get {
          path("phone" / LongNumber) { id =>
            onComplete(getUser(id)) { done =>
              cors.corsHandler(complete(done.get.get.toJSONString))
            }
          }
        },delete{
          path("phone" / LongNumber){ id =>
            onComplete(deleteUser(id)) { done =>
              cors.corsHandler(complete("OK"))
            }
          }
        },post{
            path("phones" / "createNewPhone"){
              entity(as [UserPayload]){ payload =>
                var added = addUsers(payload)
                onComplete(added) { done =>
                  var ids = done.get
                  cors.corsHandler(complete(JSONArray(ids).toString()))
                }
              }
            }
        },post{
          path("phone" / LongNumber){ id=>
            entity(as [User]){ payload =>
              var result = updateUser(id, payload)
              onComplete(result) { done =>
                cors.corsHandler(complete("updated"))
              }
            }
          }
        },options{
            pathPrefix("") {
              cors.corsHandler(complete("OK"))
            }
        },get {
          pathPrefix("phones" / "searchByName") {
            parameters('nameSubstring) { nameSubstring =>
              var result = getUserByName(nameSubstring)
              onComplete(result){ usersArr =>
                var helper: DataHelper = new DataHelper
                var users = usersArr.get
                cors.corsHandler(complete(helper.getJSONArray(users)))
              }
            }
          }
        },get {
          pathPrefix("phones" / "searchByNumber") {
            parameters('phoneSubstring) { phoneSubstring =>
              var result = getUserByPhone(phoneSubstring)
              onComplete(result){ usersArr =>
                var helper: DataHelper = new DataHelper
                var users = usersArr.get
                cors.corsHandler(complete(helper.getJSONArray(users)))
              }
            }
          }
        },get {
          pathPrefix("phones") {
              var result = getUsers()
              onComplete(result){ usersArr =>
                var helper: DataHelper = new DataHelper
                var users = usersArr.get
                cors.corsHandler(complete(helper.getJSONArray(users)))
              }

          }
        },get {
          path("initdb") {
            DB.DBWorker.initdb()
            cors.corsHandler(complete("completed"))
          }
        }
      )

    val bindingFuture = Http().bindAndHandle(route, "localhost", 8080)
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())

  }
}