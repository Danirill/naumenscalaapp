name := "NaumenScalaServer"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(

  "com.typesafe.akka" %% "akka-http"            % "10.1.5",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.5",
  "com.typesafe.akka" %% "akka-http-xml"        % "10.1.5",
  "com.typesafe.akka" %% "akka-stream"          % "2.5.18",

  "mysql"              % "mysql-connector-java" % "5.1.34",
  "org.scalikejdbc"    %% "scalikejdbc"         % "3.4.1",
  "ch.qos.logback"     %  "logback-classic"     % "1.2.3",

  "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test,
)