# Запуск сервера

Проект работает с базой данных **MySQL**. 
1. Создайте пустую таблицу с названием "test" в вашей базе данных
2. Внесите изменения в файл `/NaumenScalaServer/src/main/scala/app.conf`
```
database {
  url = "jdbc:mysql://localhost:3306/<Название вашей базы данных>"
  user = "<Пользователь>"
  password = "<Пароль>"
  ...
}
```
3. Запуситите проект в Intellij Idea и запустите файл `Webserver.scala`
4. Сделайте первый запрос - `http://localhost:8080/initdb`
> Если по каким-то причинам таблица не создалась, выполните SQL команду:
>```sql
>CREATE TABLE test.`phones` ( id int(11) auto_increment NOT NULL PRIMARY KEY, name varchar(100) NOT NULL, phone varchar(100) NOT NULL) ENGINE=InnoDB DEFAULT >CHARSET=utf8 COLLATE=utf8_general_ci;
>```
5. Теперь все готово, можете смотреть список запросов в разделе **API**

# Запуск React проекта

1. Откройте папку `db_monitor`
2. Из текущей директории введите команду `npm start` для запуска проекта
> Обратите внимание, что для запуска понадобится [Node.js](https://www.digitalocean.com/community/tutorials/node-js-ubuntu-18-04-ru), а также несколько других пакетов. 
> Для автоматической установки из директории используйте `npm install`
> * [React.js](https://www.npmjs.com/package/react),
> * [Typescript](https://www.npmjs.com/package/typescript),
> * [Axios](https://www.npmjs.com/package/axios),
> * [React-Bootstrap](https://www.npmjs.com/package/react-bootstrap)

# API
#### Добавление пользователей
```POST http://localhost:8080/phones/createNewPhone```
```
    BODY
    "users":[
        {"username":String, "userphone":String}
    ]
    RESPONSE
    [id(int), id(int)]
```
#### Получить всех пользователей
```GET http://localhost:8080/phones```
#### Получить данные одного пользователя
```GET http://localhost:8080/phone/<id>```
#### Удалить пользователя
```DELETE http://localhost:8080/phone/<id>```
#### Изменить пользователя
```POST http://localhost:8080/phone/<id>```
```
BODY
{"username":String, "userphone":String}
```
#### Поиск по имени пользователя
```GET http://localhost:8080/phones/searchByName?nameSubstring=<String>```
### Поиск по номеру пользователя
```GET http://localhost:8080/phones/searchByName?searchByNumber=<String>```

# Доработки
* Не удалось сделать правильную валидацию данных, в будущем стоит о ней позаботиться
* SQL запросы сделаны неправильно, они очень плохо редактируются.(Но даже так защищены от иньекций). Scalalikejdbc единственная библиотека, с которой удалось по-быстрому подружиться
* Еще бы разобраться со структурой проекта. Сейчас не получилось красиво расположить код, все-таки в Scala я полный новичок.

# Вместо заключения
Проект я сделал за несколько дней, почти в формате хакатона. Будет интересно в дальнейшем поработать со Scala. В документации было найдено много интересных вещей)
Хорошего дня <3

P.S Если проект не запускается, сделал zip на всякий случай: https://drive.google.com/file/d/1MqJQE_2V6fC_9GQm4GwsIancexMQ0nJV/view?usp=sharing
















